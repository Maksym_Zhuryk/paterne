from dataclasses import dataclass
from collections import defaultdict


@dataclass
class Team:
    id: int
    name: str
    member_list: list
    supplementary_materials: defaultdict
    project_id: int
