from dataclasses import dataclass


@dataclass
class PersonalInfo:
    id: int
    name: str
    address: str
    phone: str
    email: str
    position: str
    rank: str
    salary: float
