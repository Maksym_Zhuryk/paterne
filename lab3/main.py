from employees import Developer, TopManagement, ChiefTechnicalOfficer
from project import Project, MobileApp, ProjectFlow
from datetime import datetime
from personalInfo import PersonalInfo
from employees import ProjectManager


if __name__ == '__main__':
    developer = Developer(PersonalInfo(id=1,
                        name='Name1',
                        address='Bamboleylo 233',
                        phone='380994232233',
                        email='test@test.test',
                        position='junior',
                        rank='0.7',
                        salary=54000.00))

    proj_manager = ProjectManager(PersonalInfo(id=2,
                        name='Name2',
                        address='Sammbuka 303',
                        phone='380763212233',
                        email='test@test.test',
                        position='-*-',
                        rank='0.3',
                        salary=45000.00))

    top_management = TopManagement(PersonalInfo(id=3,
                        name='Name3',
                        address='Bamboleylo 2213',
                        phone='3809321223233',
                        email='test3@test.test',
                        position='-**-',
                        rank='0.7',
                        salary=54000.00))

    chef_tech_officer = ChiefTechnicalOfficer(PersonalInfo(id=37,
                                                           name='Name1',
                                                           address='Bamboleylo 2233',
                                                           phone='380991232233',
                                                           email='test3@test.test',
                                                           position='-**-',
                                                           rank='0.7',
                                                           salary=54000.00))

    project = Project(title='test', start_date=datetime.now())
    mob_app = MobileApp(title='test', start_date=datetime.now(), operating_sys='Android')
    project_flow = ProjectFlow(title='2test2', start_date=datetime.now(),
                               member_flow=24, flow_percentage=0.50)
    title = "2test2"
    start_date = datetime.now()
    member_flow = 24
    flow_percentage = 0.50

    print(mob_app.send_supplementary_materials(1, "Using sys Android 11.0.4"))
    print(chef_tech_officer.attach_project('2test2', datetime.now(), 24, 0.50))
    print(chef_tech_officer.project)
    #developer.assign(project=project)

    print(top_management.attach_project(mob_app))

    #developer.assign_possibility(project)
    #developer.assigned_projects(developer)
    #developer.unassign(project)

    proj_manager.discuss_progress(developer)

    #developer.unassign(project)
    #developer.assign_possibility(project)

    project.add_member('Morgan')


