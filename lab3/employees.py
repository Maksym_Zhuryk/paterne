from employee import Employee
from personalInfo import PersonalInfo
from task import Task
from project import Project, MobileApp, WebApp, ProjectFlow
from abc import ABC
from team import Team
from datetime import datetime
import random


class Developer(Employee):
    def __init__(self, personal_info: PersonalInfo):
        super().__init__(personal_info=personal_info)
        self.assignments = []

    def set_task(self, task: Task):
        for assignment in self.assignments:
            if assignment.project.title == task.related_project:
                assignment.project.task_list.append(task)
#                assignment.add_task(task)

    def calculate_tax(self) -> float:
        return (1500 * 30) * 0.05

    def calculate_salary(self) -> int:
        return 1500 * 30


class ProjectManager(Employee):
    def __init__(self, personal_info: PersonalInfo):
        super().__init__(personal_info=personal_info)

    def calculate_tax(self) -> float:
        return (1000 * 30) * 0.05

    def calculate_salary(self) -> int:
        return 1000 * 30

    def discuss_progress(self, developer: Employee):
        progress_list = ['good job', 'bad work, fix this', 'this is awful, remake it all']
        print(f'Oh, {developer.personal_info.name},', random.choice(progress_list))


class QualityAssurance(Employee):
    def __init__(self, personal_info: PersonalInfo):
        super().__init__(personal_info=personal_info)

    def calculate_tax(self) -> float:
        return (700 * 30) * 0.05

    def calculate_salary(self) -> int:
        return 700 * 30


class TeamLead(Employee):
    def __init__(self, personal_info: PersonalInfo):
        super().__init__(personal_info=personal_info)

    def calculate_tax(self) -> float:
        return (950 * 30) * 0.04

    def calculate_salary(self) -> int:
        return 950 * 30


class TopManagement(ABC):
    def __init__(self, personal_info: PersonalInfo, ):
        self.personal_info = personal_info
        self.list_projects = []

    def fill_project(self, team_lead: TeamLead, team: Team) -> None:
        pass


    def attach_project(self, *args) -> None:
        pass

    @staticmethod
    def calculate_tax(self) -> float:
        return (850 * 30) * 0.03

    @staticmethod
    def calculate_salary(self) -> int:
        return 850 * 30


class ChiefTechnicalOfficer(TopManagement):
    def __init__(self, personal_info: PersonalInfo):
        super().__init__(personal_info=personal_info)
        self.project = []

    def attach_project(self, *args) -> list[ProjectFlow]:
        title, start_date, member_flow, flow_percentage = args
        return [ProjectFlow(title, start_date, member_flow, flow_percentage)]

    def __str__(self) -> str:
        return f"{self.title}, {self.start_date}, {self.member_flow}, {self.slow_percentage}"


class SolutionArchitect(TopManagement):
    def __init__(self, personal_info: PersonalInfo):
        super().__init__(personal_info=personal_info)

    def attach_project(self, *args) -> list[WebApp, MobileApp]:
        title, start_date, operating_sys, kind_of_web, ip = args
        return [WebApp(title, start_date, kind_of_web, ip), MobileApp( title, start_date, operating_sys)]

'''
class Test:
    def __init__(self, a=1, name='Mobile Project') -> None:
        self.name = name
        self.a = a

    def __repr__(self) -> str:
        return f"Test({self.a})"

    def __str__(self) -> str:
        return f"Object of class Test has attribute a={self.a}, name={self.name}"

    def do_something(self, **kwargs):
        x = kwargs.get('x')
        print(x)  
        
    Test(1)test.do_something(x=1, y=1)

   
'''