from datetime import datetime
from abc import abstractmethod
from personalInfo import PersonalInfo


class Project:
    def __init__(self, title: str, start_date: datetime) -> None:
        self.title = title
        self.start_date = start_date
        self.task_list = []
        self.members = []
        self.qa = []
        self.project_manager = []

    @abstractmethod
    def add_member(self, personal_info:PersonalInfo) -> None:
        pass

    @abstractmethod
    def remove_member(self, personal_info:PersonalInfo) -> None:
        pass

    @abstractmethod
    def send_supplementary_materials(self, task_id: int, material: str) -> None:
        pass


class WebApp(Project):
    def __init__(self, title: str, start_date: datetime, kind_of_web: str,
                 ip: str) -> None:
        super().__init__(title, start_date)
        self.kind_of_web = kind_of_web
        self.ip = ip
        self.members = []

    def add_member(self, personal_info: PersonalInfo) -> None:
        self.members.append(personal_info)
        return print("member was added")

    def remove_member(self, personal_info: PersonalInfo) -> None:
        self.members.remove(personal_info)
        return print("member was removed")

    def send_supplementary_materials(self, task_id: int, material: str) -> str:
        material = "some link"
        return material


class MobileApp(Project):
    def __init__(self, title: str, start_date: datetime, operating_sys: str) -> None:
        super().__init__(title, start_date)
        self.operating_sys = operating_sys
        self.members = []

    def add_member(self, personal_info: PersonalInfo) -> None:
        self.members.append(personal_info)
        return print("member was added")

    def remove_member(self, personal_info: PersonalInfo) -> None:
        self.members.remove(personal_info)
        return print("member was removed")

    def send_supplementary_materials(self, task_id: int, material: str) -> str:
        material = "some link"
        return material


class ProjectFlow(Project):
    def __init__(self, title='test', start_date=datetime.now(), member_flow=24,
                 flow_percentage=0.57) -> None:
        super().__init__(title, start_date)
        self.member_flow = member_flow
        self.flow_percentage = flow_percentage
        self.members = []

    def add_member(self, personal_info: PersonalInfo) -> None:
        self.members.append(personal_info)
        return print("member was added")

    def remove_member(self, personal_info: PersonalInfo) -> None:
        self.members.remove(personal_info)
        return print("member was removed")

    def send_supplementary_materials(self, task_id: int, material: str) -> str:
        material = "some link"
        return material
